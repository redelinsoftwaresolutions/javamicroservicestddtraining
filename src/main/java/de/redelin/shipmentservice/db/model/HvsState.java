package de.redelin.shipmentservice.db.model;

public class HvsState implements ICarrierState {

	String stateCode;
	String stateCodeText;

	public HvsState(String stateCode, String stateCodeText) {
		super();
		this.stateCode = stateCode;
		this.stateCodeText = stateCodeText;
	}

	@Override
	public String getStateMessage() {
		return stateCodeText;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateCodeText() {
		return stateCodeText;
	}

	public void setStateCodeText(String stateCodeText) {
		this.stateCodeText = stateCodeText;
	}

}
