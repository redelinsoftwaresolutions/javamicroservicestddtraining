package de.redelin.shipmentservice.db.model;

public class DhlState implements ICarrierState {

	String iceEvent;
	String iceRic;
	String iceEventText;
	String iceRicText;

	public DhlState(String iceEvent, String iceRic, String iceEventText, String iceRicText) {
		super();
		this.iceEvent = iceEvent;
		this.iceRic = iceRic;
		this.iceEventText = iceEventText;
		this.iceRicText = iceRicText;
	}

	@Override
	public String getStateMessage() {
		return this.iceEventText + " :: " + iceRicText;
	}

	public String getIceEvent() {
		return iceEvent;
	}

	public void setIceEvent(String iceEvent) {
		this.iceEvent = iceEvent;
	}

	public String getIceRic() {
		return iceRic;
	}

	public void setIceRic(String iceRic) {
		this.iceRic = iceRic;
	}

	public String getIceEventText() {
		return iceEventText;
	}

	public void setIceEventText(String iceEventText) {
		this.iceEventText = iceEventText;
	}

	public String getIceRicText() {
		return iceRicText;
	}

	public void setIceRicText(String iceRicText) {
		this.iceRicText = iceRicText;
	}

}
