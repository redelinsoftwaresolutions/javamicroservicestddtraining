package de.redelin.shipmentservice.db.model;

/**
 * defines public methods to be provided by different carrierstates
 * 
 * @author svenredelin
 *
 */
public interface ICarrierState {
	String getStateMessage();
}
