package de.redelin.shipmentservice.db.model;

import java.util.Calendar;

public class ShipmentState {
	Calendar timestamp;
	ICarrierState carrierState;

	public ShipmentState(Calendar timestamp, ICarrierState carrierState) {
		super();
		this.timestamp = timestamp;
		this.carrierState = carrierState;
	}

	public Calendar getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Calendar timestamp) {
		this.timestamp = timestamp;
	}

	public ICarrierState getCarrierState() {
		return carrierState;
	}

	public void setCarrierState(ICarrierState carrierState) {
		this.carrierState = carrierState;
	}

}
