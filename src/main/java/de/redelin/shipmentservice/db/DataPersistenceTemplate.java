package de.redelin.shipmentservice.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Configuration;

import de.redelin.shipmentservice.db.model.DhlState;
import de.redelin.shipmentservice.db.model.HvsState;
import de.redelin.shipmentservice.db.model.ICarrierState;
import de.redelin.shipmentservice.response.TestMessage;

/**
 * dummy template to play the database-things ;)
 * 
 * @author svenredelin
 *
 */
@Configuration
public class DataPersistenceTemplate {
	static Map<String, DhlState> dhlStates;
	static Map<String, HvsState> hvsStates;

	List<TestMessage> testMessages = new ArrayList<>();

	public DataPersistenceTemplate() {
		initialize();
	}

	/**
	 * static initialization
	 */
	private static void initialize() {
		dhlStates = new HashMap<>();
		dhlStates.put("abc::123", new DhlState("abc", "123", "Daten avisiert", "elektronische Avisierung"));
		dhlStates.put("def::222", new DhlState("def", "222", "Sendung in Verteilzentrum", "Verladung"));
		dhlStates.put("ghi::333", new DhlState("ghi", "333", "Sendung wurde zugestellt", "an Ablageort abgelegt"));
		
		hvsStates = new HashMap<>();
		hvsStates.put("yxcvb", new HvsState("yxcvb", "Sendung im Hub sortiert"));
		hvsStates.put("asdfg", new HvsState("asdfg", "Sendung in Zustellung"));
		hvsStates.put("qwertz", new HvsState("qwertz", "Sendung zugestellt"));
	}

	/**
	 * search for DhlState
	 * 
	 * @param iceEvent
	 * @param iceRic
	 * @return implementation of ICarrierState
	 */
	public ICarrierState findDhlState(String iceEvent, String iceRic) {
		return dhlStates.get(iceEvent + "::" + iceRic);
	}

	/**
	 * search for HvsState
	 * 
	 * @param stateCode
	 * @return implementation of ICarrierState
	 */
	public ICarrierState findHvState(String stateCode) {
		return hvsStates.get(stateCode);
	}

	/**
	 * save test message in database (list within this bean)
	 * 
	 * @param message
	 */
	public void save(TestMessage message) {
		if (testMessages.contains(message)) {
			testMessages.remove(message);
		}
		testMessages.add(message);
	}

	/**
	 * get all test messages from database (list within this bean)
	 * 
	 * @return list of TestMessages
	 */
	public List<TestMessage> getAllTestMessages() {
		return testMessages;
	}

}
