package de.redelin.shipmentservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.redelin.shipmentservice.response.TestMessage;
import de.redelin.shipmentservice.service.TestMessageService;

/**
 * shipment service controller represents the request methods for this service
 * and starts business application
 * 
 * @RequestMapping can specify more detailed request mapping for service, but
 *                 this service will start with special port, so it will be
 *                 identified via port not special request mapping. If you want
 *                 to use the service name from application.yml you can use as
 *                 well "/${applicationName}"
 * 
 * @author sven redelin
 *
 */
@RestController
@RequestMapping("/")
public class ShipmentServiceController {

	private final TestMessageService testMessageService;

	public ShipmentServiceController(
			TestMessageService testMessageService) {
		this.testMessageService = testMessageService;
	}

	@RequestMapping(value = "/test")
	public @ResponseBody TestMessage testMessage(
			@RequestParam(value = "param", defaultValue = "ParamNotSet") String param) {

		final String textTemplate = "Your param is %s";

		TestMessage message = new TestMessage(String.format(textTemplate, param));
		testMessageService.addMessage(message);
		return message;
	}

	@RequestMapping(value = "/testList")
	public @ResponseBody List<TestMessage> testMessageList() {
		return testMessageService.getAllMessages();
	}

}
