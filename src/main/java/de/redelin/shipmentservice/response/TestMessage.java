package de.redelin.shipmentservice.response;

public class TestMessage {
	String message;

	public TestMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
