package de.redelin.shipmentservice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import de.redelin.shipmentservice.db.DataPersistenceTemplate;
import de.redelin.shipmentservice.response.TestMessage;

@Service
public class TestMessageService {
	private final DataPersistenceTemplate dbTemplate;

	public TestMessageService(DataPersistenceTemplate dbTemplate) {
		this.dbTemplate = dbTemplate;
	}

	public List<TestMessage> getAllMessages() {
		return dbTemplate.getAllTestMessages();
	}

	public void addMessage(TestMessage message) {
		dbTemplate.save(message);
	}

}
