package de.redelin.shipmentservice.service;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;

import de.redelin.shipmentservice.db.DataPersistenceTemplate;
import de.redelin.shipmentservice.response.TestMessage;

public class TestMessageServiceTest {

	@Test
	public void testHandleTestMessage() {
		TestMessage tm = new TestMessage("bla bla");
		List<TestMessage> tml = new ArrayList<>();
		tml.add(tm);

		DataPersistenceTemplate dbTemplate = EasyMock.createMock(DataPersistenceTemplate.class);
		// expect save call for exactly this message with easymock
		dbTemplate.save(tm);
		// expect prepared test list if getAllTestMessages called
		EasyMock.expect(dbTemplate.getAllTestMessages()).andReturn(tml);

		// play the mocks
		EasyMock.replay(dbTemplate);

		TestMessageService service = new TestMessageService(dbTemplate);
		service.dbTemplate = dbTemplate;
		service.addMessage(tm);
		List<TestMessage> actualTml = service.getAllMessages();

		Assert.assertEquals(tml, actualTml);

		EasyMock.verify(dbTemplate);
	}

}
