package de.redelin.shipmentservice.db;

import static org.junit.Assert.*;

import org.junit.Test;

import de.redelin.shipmentservice.db.model.ICarrierState;

public class DataPersistenceTemplateTest {

	@Test
	public void testSearchDhlState() {
		DataPersistenceTemplate template = new DataPersistenceTemplate();
		ICarrierState state = template.findDhlState("abc", "123");

		assertNotNull(state);
		assertEquals("Daten avisiert :: elektronische Avisierung", state.getStateMessage());
	}

	@Test
	public void testSearchDhlStateNegative() {
		DataPersistenceTemplate template = new DataPersistenceTemplate();
		ICarrierState state = template.findDhlState("xyz", "123");

		assertNull(state);
	}

	@Test
	public void testSearchHvsState() {
		DataPersistenceTemplate template = new DataPersistenceTemplate();
		ICarrierState state = template.findHvState("qwertz");

		assertNotNull(state);
		assertEquals("Sendung zugestellt", state.getStateMessage());
	}

	@Test
	public void testSearchHvsStateNegative() {
		DataPersistenceTemplate template = new DataPersistenceTemplate();
		ICarrierState state = template.findHvState("olkjh");

		assertNull(state);
	}

}
